import {Directive, DoCheck, ElementRef, Input, OnInit, Renderer} from '@angular/core';

@Directive({
  selector: '[i18n], [i18n-title]'
})
export class TranslationDirective implements OnInit, DoCheck{

  private map: Map<string,string>;

  @Input('i18n') key: string;
  @Input('i18n-title') title: string; //may have to create a seperate inherited directive..??

  constructor(private el: ElementRef,
              private renderer: Renderer) {
    this.map = new Map()
      .set("welcome","¡Hola i18n!")
      .set("no-output-1","No genero ningún elemento")
      .set("no-output-2","Yo tampoco genero ningún elemento")
      .set("logo","Logo de Angular")
      .set("wolf","una horda de lobos")
      .set("hero-is","El heroe es")
      .set("male-female","hombre-mojer")
      .set("fname","william")
      .set("lname","hendry");
  }

  ngOnInit(): void {

   // this.myFn();
  }

  ngDoCheck(): void {
    this.myFn();
  }


  private myFn() {
    let value = this.map.get(this.key)

    console.log(this.el.nativeElement);
    switch (this.el.nativeElement.className) {
      case HTMLLabelElement :
        this.renderer.setElementProperty(this.el.nativeElement, 'innerHTML', value + "innerHtml");
      case HTMLInputElement:
        this.renderer.setElementProperty(this.el.nativeElement, 'value', value);
    }
    console.log("init -- key:" + this.key + " | value:" + value);
    console.log("init -- title:" + this.title + " | value:" + this.map.get(this.title));
    //this.el.nativeElement.innerHTML = value;
    this.renderer.setElementProperty(this.el.nativeElement, 'innerHTML', value + "innerHtml")
    this.renderer.setElementProperty(this.el.nativeElement, 'value', value);
    //this.renderer.setElementStyle(this.el.nativeElement, 'backgroundColor', 'gray');

    //THIS WORKS too..
    let key1 = this.el.nativeElement.getAttribute("i18n");
    console.log("key1:" + key1);
  }








}
